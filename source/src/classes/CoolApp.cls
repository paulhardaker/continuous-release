global class CoolApp
{
	private static final String VERSION = '1.0';
	
	public void CoolApp()
	{
		System.debug('CoolApp constructed');
		feature1();
		feature2(); // Oops we forgot to call our new feature!
	}

	public void feature1()
	{
		System.debug('feature1 begins life');
		System.debug('feature1 grows');
	}

	public void feature2()
	{
		System.debug('feature2 begins life');
		System.debug('feature2 grows');
		System.debug('feature2 finished!');
	}
}